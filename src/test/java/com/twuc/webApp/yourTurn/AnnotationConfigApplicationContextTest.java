package com.twuc.webApp.yourTurn;

import com.twuc.other.OutOfScanningScope;
import com.twuc.webApp.configuration.Logger;
import com.twuc.webApp.configuration.MyLogger;
import com.twuc.webApp.model.*;
import com.twuc.webApp.service.InterfaceOne;
import com.twuc.webApp.service.SimpleInterface;
import com.twuc.webApp.service.impl.InterfaceOneImpl;
import com.twuc.webApp.service.impl.SimpleObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;


import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;


public class AnnotationConfigApplicationContextTest {

    private AnnotationConfigApplicationContext context;

    @BeforeEach
    void setUp() {
        context = new AnnotationConfigApplicationContext("com.twuc.webApp");
    }

    @Test
    void should_return_bean_object_with_AnnotationConfigApplicationContext() {
        WithoutDependency bean = context.getBean(WithoutDependency.class);

        assertNotNull(bean);
        assertEquals("WithoutDependency", bean.getClass().getSimpleName());
    }

    @Test
    void should_return_with_dependency_with_AnnotationConfigApplicationContext() {
        WithDependency bean = context.getBean(WithDependency.class);
        assertNotNull(bean);
        assertEquals("WithDependency", bean.getClass().getSimpleName());
    }

    @Test
    void should_throw_error_when_object_out_of_base_package() {
        assertThrows(NoSuchBeanDefinitionException.class, () -> context.getBean(OutOfScanningScope.class));
    }

    @Test
    void should_return_the_object() {
        InterfaceOneImpl bean = (InterfaceOneImpl) context.getBean(InterfaceOne.class);
        assertNotNull(bean);
    }

    @Test
    void should_return_the_bean_and_bean_name() {
        SimpleObject bean = (SimpleObject) context.getBean(SimpleInterface.class);
        SimpleDependent simpleDependent = bean.getSimpleDependent();
        assertEquals("O_o", simpleDependent.getName());
    }

    @Test
    void should_call_correct_construction() {
        MyLogger contextBean = context.getBean(MyLogger.class);
        MultipleConstructor bean = context.getBean(MultipleConstructor.class);
        assertEquals("This is dependent Log", contextBean.getLines().get(0));
    }

    @Test
    void should_return_log_message() {
        Logger bean = context.getBean(Logger.class);
        WithAutowiredMethod contextBean = context.getBean(WithAutowiredMethod.class);
        assertEquals("construction call", bean.getLines().get(0));
        assertEquals("initialize call", bean.getLines().get(1));
    }



    @Test
    void should_return_bean_array() {
        Collection<SimpleInterface> values = context.getBeansOfType(SimpleInterface.class).values();
        assertEquals(1, values.size());
    }
}
