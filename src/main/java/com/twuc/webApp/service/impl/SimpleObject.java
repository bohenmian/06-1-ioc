package com.twuc.webApp.service.impl;

import com.twuc.webApp.model.SimpleDependent;
import com.twuc.webApp.service.SimpleInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SimpleObject implements SimpleInterface {

    private SimpleDependent simpleDependent;

    @Autowired
    public SimpleObject(SimpleDependent simpleDependent) {
        this.simpleDependent = simpleDependent;
    }

    public SimpleDependent getSimpleDependent() {
        return simpleDependent;
    }
}
