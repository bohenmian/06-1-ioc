package com.twuc.webApp.service;

import com.twuc.webApp.model.SimpleDependent;

public interface SimpleInterface {

    SimpleDependent getSimpleDependent();
}
