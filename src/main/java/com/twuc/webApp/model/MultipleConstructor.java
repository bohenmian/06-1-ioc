package com.twuc.webApp.model;

import com.twuc.webApp.configuration.MyLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class MultipleConstructor {

    private Dependent dependent;
    private String name;
    private MyLogger logger;

    @Autowired
    public MultipleConstructor(Dependent dependent, MyLogger logger) {
        this.logger = logger;
        this.dependent = dependent;
        logger.markLog("This is dependent Log");
    }

    public MultipleConstructor(String name) {
        this.name = name;
        logger.markLog("This is String log");
    }
}
