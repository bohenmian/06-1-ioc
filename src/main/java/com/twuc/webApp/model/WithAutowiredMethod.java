package com.twuc.webApp.model;

import com.twuc.webApp.configuration.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class WithAutowiredMethod {

    private Dependent dependent;
    private Logger logger;
    private AnotherDependent anotherDependent;

    @Autowired
    public void initialize(AnotherDependent anotherDependent, Logger logger) {
        this.anotherDependent = anotherDependent;
        this.logger = logger;
        logger.markLog("initialize call");
    }

    public WithAutowiredMethod(Logger logger) {
        logger.markLog("construction call");
    }
}
