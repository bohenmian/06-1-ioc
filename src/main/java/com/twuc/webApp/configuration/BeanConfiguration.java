package com.twuc.webApp.configuration;

import com.twuc.webApp.model.SimpleDependent;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfiguration {

    @Bean
    public SimpleDependent getSimpleDependentBean() {
        SimpleDependent simpleDependent = new SimpleDependent();
        simpleDependent.setName("O_o");
        return simpleDependent;
    }

}
